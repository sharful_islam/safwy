// ConfigService is an app-wide singleton and instantiated once
// IFF shared.module follows the `forRoot` pattern.
//
// If it didn't, a new instance of ConfigService would be created
// after each lazy load and the configName would double up.
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';

import { Injectable, Optional } from '@angular/core';
import { HttpClient, HttpHeaders }    from '@angular/common/http';

let nextId = 1;

export class Config {
  _config;
  configName = 'Sherlock Holmes';
  public get value() : object {
    return this._config;
  }
  constructor(){
    /*this.get = function(){
      return this._config;
    }
    this.set = function(c){
       this._config = c;
      return;
    }*/
  }
}

@Injectable()
export class ViewConfig {
  _viewConfig = {
    title: 'Safeway'
  }
}

@Injectable()
export class ConfigService {
  vm = {
    labels: [],
    config: null,
    viewConfig: null,
    navItem: null,
    currentConfig: null
  };

  id = nextId++;
  private _configName = 'Sharful Islam';
  private configUrl = 'assets/config.json';
  private labelsUrl = 'assets/labels.json';

  constructor(@Optional() config: Config, @Optional() viewConfig: ViewConfig, private http: HttpClient) {
    if (config) { 
      console.warn(config);
      this._configName = config.configName; 
    }
    if (viewConfig) {
      console.warn(viewConfig)
    }
  }

  setViewConfig(currentConfig) {
    if (currentConfig){
      this.vm.viewConfig = currentConfig;
      return;
    } 
  }

  setNavigation(navItem) {
     if (navItem.link){
      this.vm.navItem = navItem;
      if(this.vm.config){
        this.vm.viewConfig = this.vm.config[navItem.link]
      }
      return;
     }
  }

  
  public setConfig(config) {
    if (config){
      this.vm.config = config;
      return;
    } 
  }

  public getViewConfig() {
      return this.vm.viewConfig;
   }

  public getConfig():Observable<Config[]> {
    if (this.vm.config && this.vm.config.length>0){
      return this.vm.config;
    } else {
       return this.http.get<Config[]>(this.configUrl);
    }
    /*
    return this.http.get<Config[]>(this.configUrl);
    */
    console.warn(this.vm.config)
  }

  public getLabels():Observable<Config[]> {
    /*if (this.vm.labels && this.vm.labels.length>0){
      return this.vm.labels;
    } else {
       this.vm.labels = this.http.get<Config[]>(this.configUrl);
       return this.vm.labels; 
    }
    // */
    return this.http.get<Config[]>(this.labelsUrl);
  }

  ngOnData () {
    console.warn('Config Init')
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
   
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
   
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
   
      // Let the app keep running by returning an empty result.
      return;
    };
  }
}
