import { ModuleWithProviders, Injectable, NgModule, Optional, SkipSelf } from '@angular/core';
import { AppBootstrapModule } from '../app-bootstrap.module';

import { CommonModule } from '@angular/common';

import { TitleComponent } from './title.component';
import { UserServiceConfig } from './user.service';
import { ConfigService } from './config.service';


@NgModule({
  imports:      [ CommonModule ],
  declarations: [ TitleComponent ],
  exports:      [ TitleComponent ],
  providers:    [ UserServiceConfig, ConfigService ]
})


@Injectable()
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  /*static forRoot(private userConfig: UserServiceConfig, private configService: ConfigService): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        {provide: UserServiceConfig, useValue: userConfig },
        {provide: ConfigService, useValue: configService }
      ]
    };
  }*/
}
