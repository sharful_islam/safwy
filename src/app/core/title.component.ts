import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-title',
  styleUrls: ['./title.component.scss'],
  templateUrl: './title.component.html',
})
export class TitleComponent implements OnInit {
  // title = 'Welcome to Safeway Associates';

  constructor() {
    // this.user = userService.userName;
    // this.title = 'Welcome to Safeway Associates';
  }

  ngOnInit() {
    // console.warn(this.title);
  }

  @Input('title') title : string;
}
