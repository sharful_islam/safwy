import { Component, OnInit } from '@angular/core';

import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  config;

  constructor(private configService: ConfigService) { 
  	this.config = configService.getViewConfig();
  }

  ngOnInit() {
  }

}
