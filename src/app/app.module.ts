import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule }    from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppBootstrapModule } from './app-bootstrap.module';
import { CoreModule } from './core/core.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RotatorComponent } from './rotator/rotator.component';
import { AboutModule } from './about/about.module';

import { ServicesModule } from './services/services.module';
import { HomeModule } from './home/home.module';


import { ContactModule } from './contact/contact.module';
import { ScholarshipModule } from './scholarship/scholarship.module';
import { LandingModule } from './landing/landing.module';

import { ConfigService } from './core/config.service';
import { UserServiceConfig } from './core/user.service';

import { ContentBlockComponent } from './content-block/content-block.component';
import { LayoutComponent } from './layout/layout.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RotatorComponent,
    LayoutComponent,
    ContentBlockComponent
    // ServicesComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // HttpClientInMemoryWebApiModule,
    AppBootstrapModule,
    AppRoutingModule,
    CoreModule,

    ContactModule,
    ScholarshipModule,
    ServicesModule,
    AboutModule,
    LandingModule,
    HomeModule,
    
  ],
  providers: [ConfigService, UserServiceConfig],
  bootstrap: [AppComponent],
  entryComponents: [HomeComponent],
  exports: [HttpClientModule, AppRoutingModule]
})
export class AppModule { }
