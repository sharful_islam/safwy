import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-scholarship',
  templateUrl: './scholarship.component.html',
  styleUrls: ['./scholarship.component.scss']
})
export class ScholarshipComponent implements OnInit {

  config;

  constructor(private configService: ConfigService) { 
  	this.config = configService.getViewConfig();
  }

  ngOnInit() {
  }

}
