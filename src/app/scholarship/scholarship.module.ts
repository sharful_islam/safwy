import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScholarshipRoutingModule } from './scholarship-routing.module';
import { ScholarshipComponent } from './scholarship.component';

@NgModule({
  imports: [
    CommonModule,
    ScholarshipRoutingModule
  ],
  declarations: [ScholarshipComponent]
})
export class ScholarshipModule { }
