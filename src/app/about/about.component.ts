import { Component, OnInit, OnChanges } from '@angular/core';

import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit{
  config;
  setConfig(c){
    this.config = c.about;
  }
  constructor(private configService: ConfigService) { 
    // this.config = this.configService.getViewConfig();
    this.configService.getConfig().subscribe((data) => this.setConfig(data));
  }

  ngOnInit() {
  }

}
