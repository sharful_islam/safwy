import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  config;
  setConfig(c){
    this.config = c.home;
  }
  constructor(private configService: ConfigService) { 
  	// this.config = this.configService.getViewConfig();
  	this.configService.getConfig().subscribe((data) => this.setConfig(data));
  }

  ngOnInit() {
  }
}
