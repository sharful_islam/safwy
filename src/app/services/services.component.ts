import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  config;

  constructor(private configService: ConfigService) { 
  	this.config = configService.getViewConfig();
  }

  ngOnInit() {
  }

}
