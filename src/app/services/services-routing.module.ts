import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterState, RouterModule, ActivatedRoute } from '@angular/router';
import { ServicesComponent } from './services.component';
import { UserServiceConfig } from '../core/user.service';
import { ConfigService } from '../core/config.service';


const routes: Routes = [{
	path: 'services',
	component: ServicesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ServicesRoutingModule implements OnInit {
  vm = {
  	settings: { loading: true, loaded: false, done: false},
  	config: null
  }

  setConfig(config) {
  	if (!this.vm.config){
  		this.vm.config = config;
  	  console.warn(this.vm.config);
  	};
  }	

  constructor(private configService: ConfigService, 
     state: ActivatedRoute/*
     root: ActivatedRoute = state.root;
    const child = root.firstChild;
    const id: Observable<string> = child.params.map(p => p.id *//*private childActive: ChildActivationStart*/) {
    configService.getConfig().subscribe((data) => this.setConfig(data));
    console.warn(state.toString());
    //configService.getConfig().subscribe((data) => this.setConfig(data));
  }
  
  ngOnInit(){
	console.warn(this.vm);
  }
}
