import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../../core/config.service';

@Component({
  selector: 'app-agro',
  templateUrl: './agro.component.html',
  styleUrls: ['./agro.component.scss']
})
export class AgroComponent implements OnInit {

  config;
  setConfig(c){
    this.config = c.agro;
  }
  constructor(private configService: ConfigService) { 
  	// this.config = this.configService.getViewConfig();
  	this.configService.getConfig().subscribe((data) => this.setConfig(data));
  }

  ngOnInit() {
  }
 }