import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from './services.component';

import { AgroComponent } from './agro/agro.component';
import { ManagementComponent } from './management/management.component';
import { EducationComponent } from './education/education.component';
import { WellnessComponent } from './wellness/wellness.component';

@NgModule({
  imports: [
    CommonModule,
    ServicesRoutingModule
  ],
  declarations: [ServicesComponent, AgroComponent, ManagementComponent, EducationComponent, WellnessComponent]
})
export class ServicesModule { }