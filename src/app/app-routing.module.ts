import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppBootstrapModule } from './app-bootstrap.module';

import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import { AppComponent } from './app.component';
import { ServicesComponent } from './services/services.component';
import { AgroComponent } from './services/agro/agro.component';
import { ManagementComponent } from './services/management/management.component';
import { EducationComponent } from './services/education/education.component';
import { WellnessComponent } from './services/wellness/wellness.component';

import { ContactComponent } from './contact/contact.component';
import { ScholarshipComponent } from './scholarship/scholarship.component';
import { LandingComponent } from './landing/landing.component';

import { UserServiceConfig } from './core/user.service';
import { ConfigService } from './core/config.service';

const routes: Routes = [{
		path: '',
    redirectTo: 'home',
    pathMatch: 'full'
	},{
    path: 'landing',
    redirectTo: 'home',
    pathMatch: 'full'
  },{
    path: 'home',
    component: LandingComponent
  },{
    path: 'services',
    component: ServicesComponent,
    children: [
        { path: 'agro', component: AgroComponent},
        { path: 'management', component: ManagementComponent },
        { path: 'education', component: EducationComponent }
     ]
  },{
    path: 'wellness',
    component: WellnessComponent,
    children: [
        { path: 'drugaware', component: AgroComponent},
        { path: 'telemedicine', component: ManagementComponent },
        { path: 'aidsaware', component: EducationComponent },
        { path: 'canceraware', component: WellnessComponent }
     ]
  },{
		path: 'about',
		component: AboutComponent
	},{
		path: 'contact',
		component: ContactComponent
	},{
		path: 'scholarship',
		component: ScholarshipComponent
	},
	{ path: '**', component: AppComponent }
];

@NgModule({
  imports: [AppBootstrapModule, RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule implements OnInit {
  vm = {
  	config: null,
  	labels: null,
  };

  setConfig(config) {
  	console.warn('APP Routing setConfig');
  	if (!this.vm.config){
  		this.vm.config = config;
  	} else {
  		console.warn(this.vm.config);
  		
  	}
  };

  getConfig(config) {
  	return this.vm.config;
  };

  constructor(configService: ConfigService) {
    // configService.getConfig().subscribe((data) => this.config = data.home);
    configService.getConfig().subscribe((data) => this.vm.config = data);
    configService.getLabels().subscribe((data) => this.vm.labels = data);
    //configService.setConfig().subscribe((data) => this.getConfig(data));
  };

  ngOnInit(){
  	console.warn(this.vm);
  }
}
