import _ from 'lodash';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'content-block',
  templateUrl: './content-block.component.html',
  styleUrls: ['./content-block.component.scss']
})
export class ContentBlockComponent implements OnInit {
  _content=[];
  items = []; // {"title": "Welcome to Safeway Associates", "group": "contact", "id": 3, "lede": "Education Unbound"}];

  options = {
    title: String,
    image: String,
    blurb: String,
    lede: String,
    id: Number
  }
  setContent(){
    _.each(this._content, function(i){ this.items.push(i)});

    console.warn(JSON.stringify(this._content));   
  }

  constructor() {
    this.content=this._content;
  }

  ngOnInit() {
    this.setContent(); 
  }


  @Input('content') content : object;
}
