import { NgModule, OnInit } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { UserServiceConfig } from '../core/user.service';
import { ConfigService } from '../core/config.service';


const routes: Routes = [{
	path: 'home',
	component: HomeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class HomeRoutingModule implements OnInit {
  vm = {
  	settings: { loading: true, loaded: false, done: false},
  	config: null
  }

  setConfig(config) {
  	if (!this.vm.config){
  		this.vm.config = config;
  	  console.warn(this.vm.config);
  	};
  }	

  constructor(configService: ConfigService) {
    // configService.getConfig().subscribe((data) => this.setConfig(data));
    //configService.getConfig().subscribe((data) => this.setConfig(data));
  }
  
  ngOnInit(){
	console.warn(this.vm);
  }
}
