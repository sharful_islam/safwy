import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { AppBootstrapModule } from '../app-bootstrap.module';
import { switchMap, filter } from 'rxjs/operators';

import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  pageTitle = 'Home'; 
  selectedViewConfig;
  selectedView = 'home';
  config; viewConfig; selectedNavItem; items; loading;
  rotatorItems=[];
  navBar;

	public selectedNavChange (navItem){
    this.pageTitle = navItem.title;
    this.selectedNavItem = navItem;
		console.warn(this.pageTitle);
    
    this.configService.setNavigation(this.selectedNavItem);
    this.selectedViewConfig = this.configService.getViewConfig();
		console.warn(this.selectedViewConfig.cardItems);

    this.navBar.items.find(function(link){ 
      return (link===navItem.link)
    });

	};

  setConfig(options) {
    this.config = options;
    this.navBar = options.navbar;
    
    if (this.loading===true) return;
    //configService.getConfig
    if(this.selectedView){
      this.viewConfig = this.config[this.selectedView];
      this.pageTitle = this.viewConfig.title;
      this.rotatorItems = this.viewConfig;
      console.warn(this);
    }
  }

  inNavigation(route){
    if(this.loading===true|| !route.url) return;
    this.loading = true;
    this.selectedView = route.url.split('/')[route.url.length - 1];
    console.warn('startNavigation route '+this.selectedView);
    console.warn(route.url);
    setTimeout(function(){this.loading = false}, 1000);
  }
  endNavigation(route){
      console.warn('endNavigation route');
      console.warn(route);
      this.loading = false;
  }
  constructor( 
    public configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router) {
      console.warn(this.router.events);

      this.configService.getConfig().subscribe((data) => this.setConfig(data));
      this.router.events.subscribe((data)=>this.inNavigation(data), (data)=>this.endNavigation(data));
  }

  ngOnInit() {
  	/*this.route.paramMap.pipe(
	    switchMap((params: ParamMap) =>
	      this.setActive(params.get('id'))
    this.route.paramMap.pipe(
        switchMap((params: ParamMap) => this.selectedView = params))
    );
  	console.warn(this);
	);*/
  }

  @Input('slides') slides : object;
}
