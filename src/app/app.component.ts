import { Component } from '@angular/core';
import { AppBootstrapModule } from './app-bootstrap.module';

import { ConfigService } from './core/config.service';
import { UserService } from './core/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserService, ConfigService]
})
export class AppComponent {
  title = 'safewayassoc';
  vm = {
    loading: true,
    config: null,
    labels: null,
    viewConfig: null
  };


  constructor(public configService: ConfigService) {
    configService.getConfig().subscribe((data) => this.setConfig(data));
    configService.getLabels().subscribe((data) => this.setLabels(data));
    // configService.getViewConfig().subscribe((data) => this.setViewConfig(data));
  };

  onActivate($event) {
      let toState = $event.constructor.name.split('Component')[0];
      if(toState && toState.length && this.vm.config){
        console.warn('toState: '+ toState);
        this.vm.viewConfig = this.vm.config[toState.toLowerCase()];
        try {
          this.configService.setViewConfig(this.vm.viewConfig);
        } catch(e){
          console.error(e);
        }
      }
  }

  onDeactivate($event) {
      let fromState = $event.constructor.name.split('Component')[0];
        console.warn('fromState: '+ fromState);
        this.vm.loading = false;
        try {
          // this.configService.setViewConfig(this.vm.viewConfig);
        } catch(e){
          console.error(e);
        }
  }
  
  setLabels(labels){
      this.vm.labels = labels;
      return;
  }
  
  getLabels(lang){
    return this.vm.labels;
  }

  setConfig(config) {
      this.vm.config = config;
      this.configService.setConfig(config);
  };

  setViewConfig(config) {
      this.vm.viewConfig = config;
      console.warn(config)
  };

  getConfig(config) {
    return this.vm.config;
  }

  ngOnInit(){
    console.warn('App Component Init');
  }
  OnData(data){
    console.warn(data)
  }
}