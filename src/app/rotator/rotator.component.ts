import _ from 'lodash';
import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ConfigService } from '../core/config.service';

@Component({
  selector: 'app-rotator',
  templateUrl: './rotator.component.html',
  styleUrls: ['./rotator.component.scss']
})
export class RotatorComponent implements OnInit, OnChanges {
  config; currentRoute; vm=this;
  rotatorInterval;
  slides = []; // {"title": "Welcome to Safeway Associates", "group": "contact", "id": 3, "lede": "Education Unbound"}];

  selectedSlide = {
    title: String,
    group: String,
    lede: String,
    image: String,
    id: Number
  }

  options = {
    title: this.selectedSlide.title,
    group: this.selectedSlide.group,
    lede: this.selectedSlide.lede,
    image: this.selectedSlide.image,
    id: this.selectedSlide.id
  };

  setConfig(conf){
    this.config = conf;
    if (this.config && this.config[this.currentRoute]){
      this.config.slideshow = true;
      this.viewConfig = this.config[this.currentRoute];
      this.slides = this.config[this.currentRoute].cardItems;
      this.setupSlides();
    } else {
      this.config.slideshow = false;
    }
  }
  updateSlides(){
    if (this.slides && this.currentRoute){
      this.viewConfig = this.config[this.currentRoute];
      this.slides = this.config[this.currentRoute].cardItems;
  
      let aNum = 0, l = this.slides.length;
      if (l > 1)
      _.each(this.slides, function(s, i){
        if (s.active === true){
          s.active = false;
          aNum = (i+1);
        }
      });
      

      let activeSlide = this.slides[aNum] ? this.slides[aNum]  : this.slides[0];
      if (activeSlide){
        this.selectedSlide = activeSlide;
        activeSlide.active = true;
        // console.warn(this.selectedSlide);
        // this.slides[(i+1)%l].active = true;
      }
    }
  }

  setupInterval(){
    try{
      clearInterval(this.rotatorInterval);
      this.rotatorInterval = setInterval( () => {
         this.updateSlides();
      }, 3000);
    } catch(e){
      console.error(e)
    }
         
  }
  deactivateSlides(){
    _.each(this.slides, function(s, i){
      s.active = false;
    });
  }
  setupSlides(){
    this.deactivateSlides();
    this.slides[0].active = true;
    this.setupInterval();
  }
  
  inNavigation(route){
    if(!route.url) return;
    
    // console.warn('startNavigation route 1ks'+route);

    this.currentRoute = route.url? route.url.split('/')[1] : null;

    setTimeout(function(){
      this.loading = false;
    }, 1000);
  }

  navError(route){
    if(!route.url) return;
    
    console.error('navError : '+route);

    this.currentRoute = route.url? route.url.split('/')[route.url.length] : 'home';

    setTimeout(function(){
      this.loading = false;
    }, 1000);
  }

  navComplete(){
      console.warn('navComplete : '+this.route);
  }

  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router) {
    console.warn(route)
      configService.getConfig().subscribe((data) => this.setConfig(data));
      router.events.subscribe((data) => this.inNavigation(data),(err) => this.navError(err), () => this.navComplete());
  }

  ngOnChanges() {
  }

  ngOnInit() {
  }

  @Input('config') viewConfig : object;
}
